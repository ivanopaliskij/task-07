package com.epam.rd.java.basic.task7.db;

import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public class DBManager {

	private static final String FIND_ALL_USERS = "select * from users";
	private static final String FIND_USER_BY_LOGIN = "select * from users where login = ?";
	private static final String INSERT_USER = "insert into users values(default, ?)";
	private static final String DELETE_USER = "delete from users where login = ?";


	private static final String FIND_ALL_TEAMS = "select * from teams";
	private static final String FIND_TEAM_BY_NAME = "select * from teams where name = ?";
	private static final String INSERT_TEAM = "insert into teams values(default, ?)";
	private final static String DELETE_TEAM_BY_NAME = "delete from teams where name = ?";

	private static final String FIND_TEAMS_BY_USER_ID = "SELECT * FROM teams INNER JOIN users_teams ON " +
			"id = team_id WHERE user_id = ?";
	private final static String UPDATE_TEAM = "update teams set name = ? where id = ?";
	private static final String INSERT_TEAMS_FOR_USER = "insert into users_teams values (?, ?)";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(FIND_ALL_USERS);
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				User user = User.createUser(resultSet.getString("login"));
				users.add(user);
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, resultSet);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getLogin());
			int res = statement.executeUpdate();
			if(res == 0)
				throw new DBException("Problem occurred while inserting user", new SQLException());
			resultSet = statement.getGeneratedKeys();
			if(resultSet.next()){
				int index = resultSet.getInt(1);
				user.setId(index);
			}
		} catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		} finally {
			close(connection, statement, resultSet);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(DELETE_USER);
			for(User u:users){
				statement.setString(1, u.getLogin());
				statement.executeUpdate();
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, null);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(FIND_USER_BY_LOGIN);
			statement.setString(1, login);
			resultSet = statement.executeQuery();
			if(resultSet.next()){
				user = User.createUser(resultSet.getString("login"));
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, resultSet);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(FIND_TEAM_BY_NAME);
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			if(resultSet.next()){
				team = Team.createTeam(resultSet.getString("name"));
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, resultSet);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(FIND_ALL_TEAMS);
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				Team team = Team.createTeam(resultSet.getString("name"));
				teams.add(team);
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, resultSet);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());
			int res = statement.executeUpdate();
			if(res == 0)
				throw new DBException("Problem occurred while inserting team", new SQLException());
			resultSet = statement.getGeneratedKeys();
			if(resultSet.next()){
				int index = resultSet.getInt(1);
				team.setId(index);
			}
		} catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		} finally {
			close(connection, statement, resultSet);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(INSERT_TEAMS_FOR_USER);
			connection.setAutoCommit(false);
			if (user.getId() == 0){
				statement.setNull(1, Types.INTEGER);
			}else{
				statement.setInt(1, user.getId());
			}
			for (Team t : teams) {
				if (t.getId() == 0){
					statement.setNull(2, Types.INTEGER);
				}else{
					statement.setInt(2, t.getId());
				}
				statement.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (Exception e) {
			try {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				throw new DBException(e.getMessage(), e);
			}
			throw new DBException(e.getMessage(), e);
		} finally {
			close(connection, statement, null);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teamList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(FIND_TEAMS_BY_USER_ID);
			if(user.getId() == 0){
				statement.setNull(1, Types.INTEGER);
			}else{
				statement.setInt(1, user.getId());
			}
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				teamList.add(Team.createTeam(resultSet.getString("name")));
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, resultSet);
		}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(DELETE_TEAM_BY_NAME);
			statement.setString(1, team.getName());
			statement.executeUpdate();
		} catch (Exception e) {
			throw new DBException(e.getMessage(), e);
		} finally {
			close(connection, statement, null);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(UPDATE_TEAM);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			close(connection, statement, null);
		}
		return true;
	}

	private static Properties loadProperties() throws IOException {
		Properties properties = new Properties();
		properties.load(new FileInputStream("app.properties"));
		return properties;

	}
	private static void close(Connection connection, PreparedStatement statement, ResultSet set) throws DBException {
		close(connection);
		close(statement);
		close(set);
	}
	private static void close(Connection connection) throws DBException {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception ex) {
				throw new DBException(ex.getMessage(), ex);
			}
		}
	}
	private static void close(PreparedStatement statement) throws DBException {
		if (statement != null) {
			try {
				statement.close();
			} catch (Exception ex) {
				throw new DBException(ex.getMessage(), ex);
			}
		}
	}
	private static void close(ResultSet set) throws DBException {
		if (set != null) {
			try {
				set.close();
			} catch (SQLException ex) {
				throw new DBException(ex.getMessage(), ex);
			}
		}
	}

}
